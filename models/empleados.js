var mongoose = require('mongoose');

var empleadosSchema = mongoose.Schema({
    nombre:{
        type: String
    },
    direccion:{
        type: String
    },
    
});

var empleados = mongoose.model('empleados', empleadosSchema);  //data access object DAO

module.exports = empleados;