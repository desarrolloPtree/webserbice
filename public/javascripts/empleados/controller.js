//modulo controller
angular.module('listaEmpleados', ['empleadosService'])

.controller('listaEmpleados', ['$scope', 'empleadosFactoria' ,function($scope, empleadosFactoria){

     empleadosFactoria.listaEmpleados().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
                swal(
                  'Error',
                  response.message,
                  'error'
                );
            }
    });

    $scope.prueba=function(){
        console.log($scope.nuevo)

        empleadosFactoria.nuevo($scope.nuevo).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");

                //Refresca la lista
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);

            }

        });

    }


}])