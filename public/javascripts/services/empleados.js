//factoria
angular.module('empleadosService', [])

.factory('empleadosFactoria', function($http){
   return {
      listaEmpleados: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/api/listaEmpleados',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
        nuevo: function(empleado){
           console.log("Entro a Factoria")
         var promise = $http({
               method: 'POST',
               url: '/api/nuevoEmpleado',
               data: {
                  nuevoEmpleado: empleado
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
   }
});

