var express = require('express');
var router = express.Router();
var servicioEmpleado = require('../Services/empleadoService');

/* GET home page. */
router.get('/', function(req, res, next){
   res.render('index');
});



router.post('/api/nuevoEmpleado', function(req, res, next){
    console.log("Entro a ruta")
     var empleado = req.body.nuevoEmpleado;
             servicioEmpleado.nuevoEmpleado(empleado, function(error, empleadoGuardado){
                    if(error){
                        return res.send({code:-1,message:"Error en la insercion",data:null});
                    }

                        return res.send({code:0,message:"Guardado Exitosamente",data:empleadoGuardado});
                });


});


router.get('/api/listaEmpleados', function(req, res, next) {

    	 servicioEmpleado.mostrarEmpleados(function(error, listaEmpleados){
            if(error){
                return res.send({code:-1, message:"Error al solicitar datos", data:null});
            }

                return res.send({code:0, message:"Respuesta existosa", data:listaEmpleados});

        });

});

module.exports = router;
